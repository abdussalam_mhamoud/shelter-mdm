// Localization completed
angular.module('headwind-kiosk')
    .controller('DashboardTabController', function ($scope, localization, statService) {
        $scope.stat = undefined;
        $scope.errorMessage = undefined;

        $scope.enrollmentLabels = [
            localization.localize('dashboard.devices.enrolled.earlier'),
            localization.localize('dashboard.devices.enrolled.monthly')
        ];
        $scope.enrollmentColors = [
            '#DCDCDC',
            '#97BBCD'
        ];

        $scope.statusLabels = [
            localization.localize('dashboard.devices.offline'),
            localization.localize('dashboard.devices.idle'),
            localization.localize('dashboard.devices.active')
        ];
        $scope.statusColors = [
            '#F7464A',
            '#FDB45C',
            '#46BFBD'
        ];

        $scope.installLabels = [
            localization.localize('dashboard.devices.installation.failed'),
            localization.localize('dashboard.devices.version.mismatch'),
            localization.localize('dashboard.devices.installation.completed')
        ];
        $scope.installColors = $scope.statusColors;

        statService.getDeviceStat(function (response) {
            var devicesEnrolledEarlier = response.data.devicesEnrolled - response.data.devicesEnrolledLastMonth;
            if (devicesEnrolledEarlier < 0) {
                devicesEnrolledEarlier = 0;
            }
            $scope.enrollmentData = [devicesEnrolledEarlier, response.data.devicesEnrolledLastMonth];
            $scope.statusData = [0, 0, 0];
            $scope.installData = [0, 0, 0];

            response.data.statusSummary.forEach(function (item, index) {
                if (item.stringAttr === 'red') {
                    $scope.statusData[0] = item.number;
                } else if (item.stringAttr === 'yellow') {
                    $scope.statusData[1] = item.number;
                } else if (item.stringAttr === 'green') {
                    $scope.statusData[2] = item.number;
                }
            })

            response.data.installSummary.forEach(function (item, index) {
                if (item.stringAttr === 'FAILURE') {
                    $scope.installData[0] = item.number;
                } else if (item.stringAttr === 'VERSION_MISMATCH') {
                    $scope.installData[1] = item.number;
                } else if (item.stringAttr === 'SUCCESS') {
                    $scope.installData[2] = item.number;
                }
            })

        }, function () {
            $scope.errorMessage = localization.localize('error.internal.server');
        });

    });