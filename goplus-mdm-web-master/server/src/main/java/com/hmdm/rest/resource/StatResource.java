/*
 *
 * Headwind MDM: Open Source Android MDM Software
 * https://h-mdm.com
 *
 * Copyright (C) 2019 Headwind Solutions LLC (http://h-sms.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hmdm.rest.resource;

import com.hmdm.notification.PushService;
import com.hmdm.persistence.ConfigurationDAO;
import com.hmdm.persistence.ConfigurationFileDAO;
import com.hmdm.persistence.DeviceDAO;
import com.hmdm.persistence.domain.*;
import com.hmdm.rest.json.DeviceLookupItem;
import com.hmdm.rest.json.PaginatedData;
import com.hmdm.rest.json.Response;
import com.hmdm.rest.json.StatResponse;
import com.hmdm.rest.json.view.devicelist.DeviceListView;
import com.hmdm.rest.json.view.devicelist.DeviceView;
import com.hmdm.security.SecurityContext;
import com.hmdm.security.SecurityException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Api(tags = {"Statistics"}, authorizations = {@Authorization("Bearer Token")})
@Singleton
@Path("/private/stat")
public class StatResource {

    private static final Logger log = LoggerFactory.getLogger(StatResource.class);

    private DeviceDAO deviceDAO;

    /**
     * <p>A constructor required by Swagger.</p>
     */
    public StatResource() {
    }

    @Inject
    public StatResource(DeviceDAO deviceDAO) {
        this.deviceDAO = deviceDAO;
    }

    // =================================================================================================================
    @ApiOperation(
            value = "Get device statistics",
            notes = "Get statistics on devices",
            response = StatResponse.class
    )
    @GET
    @Path("/devices")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDeviceStats() {
        StatResponse statResponse = new StatResponse();

        statResponse.setInstallSummary(deviceDAO.getInstallStat());
        if (statResponse.getInstallSummary() == null) {
            log.error("Failed to get installation statistics!");
            return Response.INTERNAL_ERROR();
        }

        statResponse.setStatusSummary(deviceDAO.getStatusStat());
        if (statResponse.getStatusSummary() == null) {
            log.error("Failed to get device status statistics!");
            return Response.INTERNAL_ERROR();
        }

        statResponse.setDevicesTotal(deviceDAO.getTotalDevicesCount());
        statResponse.setDevicesEnrolled(deviceDAO.countEnrolled(0));
        statResponse.setDevicesEnrolledLastMonth(deviceDAO.countEnrolled(
                System.currentTimeMillis() - 30 * 86400 * 1000l));

        return Response.OK(statResponse);
    }

}
