package com.hmdm.rest.json;

import java.util.List;

public class StatResponse {
    private List<StatItem> statusSummary;
    private List<StatItem> installSummary;
    private long devicesTotal;
    private long devicesEnrolled;
    private long devicesEnrolledLastMonth;

    public List<StatItem> getStatusSummary() {
        return statusSummary;
    }

    public void setStatusSummary(List<StatItem> statusSummary) {
        this.statusSummary = statusSummary;
    }

    public List<StatItem> getInstallSummary() {
        return installSummary;
    }

    public void setInstallSummary(List<StatItem> installSummary) {
        this.installSummary = installSummary;
    }

    public long getDevicesTotal() {
        return devicesTotal;
    }

    public void setDevicesTotal(long devicesTotal) {
        this.devicesTotal = devicesTotal;
    }

    public long getDevicesEnrolled() {
        return devicesEnrolled;
    }

    public void setDevicesEnrolled(long devicesEnrolled) {
        this.devicesEnrolled = devicesEnrolled;
    }

    public long getDevicesEnrolledLastMonth() {
        return devicesEnrolledLastMonth;
    }

    public void setDevicesEnrolledLastMonth(long devicesEnrolledLastMonth) {
        this.devicesEnrolledLastMonth = devicesEnrolledLastMonth;
    }
}
