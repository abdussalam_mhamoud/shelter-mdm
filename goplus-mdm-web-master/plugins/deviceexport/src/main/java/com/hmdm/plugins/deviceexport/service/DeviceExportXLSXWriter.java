/*
 *
 * Headwind MDM: Open Source Android MDM Software
 * https://h-mdm.com
 *
 * Copyright (C) 2019 Headwind Solutions LLC (http://h-sms.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hmdm.plugins.deviceexport.service;

import com.google.inject.Inject;
import com.hmdm.persistence.CommonDAO;
import com.hmdm.persistence.domain.Settings;
import com.hmdm.plugins.deviceexport.persistence.domain.DeviceExportApplicationConfigurationView;
import com.hmdm.plugins.deviceexport.persistence.domain.DeviceExportApplicationDeviceView;
import com.hmdm.plugins.deviceexport.persistence.domain.DeviceExportGroupDeviceView;
import com.hmdm.plugins.deviceexport.persistence.domain.DeviceExportRecord;
import com.hmdm.util.ResourceBundleUTF8Control;
import org.apache.ibatis.cursor.Cursor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>A writer for exported device data using format of XLSX files.</p>
 */
public class DeviceExportXLSXWriter implements DeviceExportWriter {

    private CommonDAO commonDAO;

    /**
     * <p>Constructs new <code>DeviceExportXLSXWriter</code> instance. This implementation does nothing.</p>
     */
    public DeviceExportXLSXWriter() {
        // Empty
    }

    /**
     * <p>Constructs new <code>DeviceExportXLSXWriter</code> instance. This implementation does nothing.</p>
     */
    @Inject
    public DeviceExportXLSXWriter(CommonDAO commonDAO) {
        this.commonDAO = commonDAO;
    }

    /**
     * <p>Writes the data for the specified exported devices to specified output.</p>
     *
     * @param devices        a cursor over the list of the devices to be exported.
     * @param output         a stream to write the data for the exported devices to.
     * @param locale         a locale to be used for customizing the output.
     * @param configurations a mapping from configuration ID to list of applications set for the configuration.
     * @param devicesApps    a mapping from device ID to list of applications installed on device.
     * @throws IOException if an I/O error occurs while writting device data.
     */
    @Override
    public void exportDevices(Cursor<DeviceExportRecord> devices,
                              OutputStream output,
                              String locale,
                              Map<Integer, List<DeviceExportApplicationConfigurationView>> configurations,
                              Map<Integer, List<DeviceExportApplicationDeviceView>> devicesApps,
                              Map<Integer, List<DeviceExportGroupDeviceView>> devicesGroups) throws IOException {
        writeXLSX(devices, output, locale, configurations, devicesApps, devicesGroups);
    }

    /**
     * <p>Exports the specified devices into Excel workbook which is written to specified stream.</p>
     *
     * @param devices        a stream of devices to be exported.
     * @param output         a stream to write the generated content to.
     * @param locale         a locale to be used for localizing the generated content.
     * @param configurations the details for available configurations.
     * @param devicesApps    a mapping from device IDs to list of device applications.
     * @throws IOException if an I/O error occurs.
     */
    private void writeXLSX(Cursor<DeviceExportRecord> devices, OutputStream output, String locale,
                           Map<Integer, List<DeviceExportApplicationConfigurationView>> configurations,
                           Map<Integer, List<DeviceExportApplicationDeviceView>> devicesApps,
                           Map<Integer, List<DeviceExportGroupDeviceView>> devicesGroups) throws IOException {

        try (Workbook workbook = new XSSFWorkbook()) {

            Sheet sheet = workbook.createSheet("Devices");

            // Create a header row describing what the columns mean
            CellStyle boldStyle = workbook.createCellStyle();
            Font font = workbook.createFont();
            font.setBold(true);
            boldStyle.setFont(font);
            boldStyle.setAlignment(HorizontalAlignment.CENTER);

            if (locale.contains("_")) {
                locale = locale.substring(0, locale.indexOf("_"));
            }

            ResourceBundle translations = ResourceBundle.getBundle(
                    "plugin_deviceexport_translations", new Locale(locale), new ResourceBundleUTF8Control()
            );

            Settings settings = commonDAO.getSettings();

            Row headerRow = sheet.createRow(0);
            List<String> headerList = new LinkedList<String>();
            headerList.add(translations.getString("device.export.header.devicenumber"));
            headerList.add(translations.getString("device.export.header.imei"));
            headerList.add(translations.getString("device.export.header.phone"));
            headerList.add(translations.getString("device.export.header.description"));
            headerList.add(translations.getString("device.export.header.group"));
            headerList.add(translations.getString("device.export.header.configuration"));
            headerList.add(translations.getString("device.export.header.launcher.version"));
            headerList.add(translations.getString("device.export.header.permission.status"));
            headerList.add(translations.getString("device.export.header.installation.status"));
            if (settings.getCustomPropertyName1() != null && !settings.getCustomPropertyName1().trim().equals("")) {
                headerList.add(settings.getCustomPropertyName1());
            }
            if (settings.getCustomPropertyName2() != null && !settings.getCustomPropertyName2().trim().equals("")) {
                headerList.add(settings.getCustomPropertyName2());
            }
            if (settings.getCustomPropertyName3() != null && !settings.getCustomPropertyName3().trim().equals("")) {
                headerList.add(settings.getCustomPropertyName3());
            }

            addStringCells(headerRow, headerList, boldStyle);

            CellStyle commonStyle = workbook.createCellStyle();
            commonStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            commonStyle.setWrapText(true);

            AtomicInteger rowNum = new AtomicInteger(0);
            devices.forEach(device -> {
                Row row = sheet.createRow(rowNum.incrementAndGet());
                device.setApplications(devicesApps.get(device.getId()));
                addCells(device, row, translations, configurations, devicesGroups, commonStyle, settings);
            });

            sheet.setColumnWidth(0, 6000);
            sheet.setColumnWidth(1, 6000);
            sheet.setColumnWidth(2, 6000);
            sheet.setColumnWidth(3, 6000);
            sheet.setColumnWidth(4, 6000);
            sheet.setColumnWidth(5, 6000);
            sheet.setColumnWidth(6, 6000);
            sheet.setColumnWidth(7, 16000);
            sheet.setColumnWidth(8, 16000);
            int pos = 9;
            if (settings.getCustomPropertyName1() != null && !settings.getCustomPropertyName1().trim().equals("")) {
                sheet.setColumnWidth(pos, 6000);
                pos++;
            }
            if (settings.getCustomPropertyName2() != null && !settings.getCustomPropertyName2().trim().equals("")) {
                sheet.setColumnWidth(pos, 6000);
                pos++;
            }
            if (settings.getCustomPropertyName3() != null && !settings.getCustomPropertyName3().trim().equals("")) {
                sheet.setColumnWidth(pos, 6000);
                pos++;
            }

            workbook.write(output);
        }
    }

    /**
     * <p>Writes the data for specified device into specified row in Excel workbook.</p>
     *
     * @param device         a device to be exported.
     * @param row            a row in Excel workbook to write the data to.
     * @param translations   a locale to be used for localizing the generated content.
     * @param configurations the details for available configurations.
     * @param commonStyle    a style for applying to all cells.
     */
    private void addCells(DeviceExportRecord device, Row row, ResourceBundle translations,
                          Map<Integer, List<DeviceExportApplicationConfigurationView>> configurations,
                          Map<Integer, List<DeviceExportGroupDeviceView>> devicesGroups,
                          CellStyle commonStyle, Settings settings) {

        Cell numberCell = row.createCell(0, CellType.STRING);
        numberCell.setCellStyle(commonStyle);
        numberCell.setCellValue(device.getDeviceNumber());

        Cell imeiCell = row.createCell(1, CellType.STRING);
        imeiCell.setCellStyle(commonStyle);
        imeiCell.setCellValue(stripTrailingQuotes.apply(device.getImei()));

        Cell phoneNumberCell = row.createCell(2, CellType.STRING);
        phoneNumberCell.setCellStyle(commonStyle);
        phoneNumberCell.setCellValue(stripTrailingQuotes.apply(device.getPhone()));

        Cell descriptionCell = row.createCell(3, CellType.STRING);
        descriptionCell.setCellStyle(commonStyle);
        descriptionCell.setCellValue(stripTrailingQuotes.apply(device.getDescription()));

        Cell groupCell = row.createCell(4, CellType.STRING);
        groupCell.setCellStyle(commonStyle);
        groupCell.setCellValue(evaluateDeviceGroups(device, devicesGroups));

        Cell configNameCell = row.createCell(5, CellType.STRING);
        configNameCell.setCellStyle(commonStyle);
        configNameCell.setCellValue(device.getConfigurationName());

        Cell launcherVersionCell = row.createCell(6, CellType.STRING);
        launcherVersionCell.setCellStyle(commonStyle);
        launcherVersionCell.setCellValue(stripTrailingQuotes.apply(device.getLauncherVersion()));

        // Permissions status
        final List<String> permissionStatusKeys = encodePermissionsStatus.apply(device);
        StringBuilder b = new StringBuilder();
        for (String key : permissionStatusKeys) {
            if (b.length() > 0) {
                b.append("\n");
            }
            b.append(translations.getString(key));
        }

        Cell permissionsCell = row.createCell(7, CellType.STRING);
        permissionsCell.setCellStyle(commonStyle);
        permissionsCell.setCellValue(b.toString());

        // Installation status
        Cell installationsCell = row.createCell(8, CellType.STRING);
        installationsCell.setCellStyle(commonStyle);

        if (device.isInfoAvailable()) {
            final String status = evaluateDeviceAppInstallationStatus(device, translations, configurations);
            installationsCell.setCellValue(status);
        }

        int pos = 9;
        if (settings.getCustomPropertyName1() != null && !settings.getCustomPropertyName1().trim().equals("")) {
            Cell customCell1 = row.createCell(pos, CellType.STRING);
            customCell1.setCellStyle(commonStyle);
            customCell1.setCellValue(device.getCustom1());
            pos++;
        }

        if (settings.getCustomPropertyName2() != null && !settings.getCustomPropertyName2().trim().equals("")) {
            Cell customCell2 = row.createCell(pos, CellType.STRING);
            customCell2.setCellStyle(commonStyle);
            customCell2.setCellValue(device.getCustom2());
            pos++;
        }

        if (settings.getCustomPropertyName3() != null && !settings.getCustomPropertyName3().trim().equals("")) {
            Cell customCell3 = row.createCell(pos, CellType.STRING);
            customCell3.setCellStyle(commonStyle);
            customCell3.setCellValue(device.getCustom3());
            pos++;
        }
    }

    /**
     * <p>Writes the cells with specified content to specified row in Excel workbook.</p>
     *
     * @param row     a row in Excel workbook to write the data to.
     * @param strings the contents of the cells.
     * @param style   a style to be applied to cells.
     */
    private static void addStringCells(Row row, List<String> strings, CellStyle style) {
        for (int i = 0; i < strings.size(); i++) {
            Cell cell = row.createCell(i, CellType.STRING);
            cell.setCellValue(strings.get(i));
            cell.setCellStyle(style);
        }
    }
}
